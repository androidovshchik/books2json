export interface Database {
  config: Config;
  categories: Category[];
}

export interface Config {
  token: string;
  datetime: string;
  version: string;
}

export interface Category {
  id: string;
  name: string;
  books: Book[];
}

export interface Book {
  id: string;
  name: string;
  author: string;
  reader: string;
  description: string;
  image: string;
  folder: string;
  duration: string;
  video: boolean;
  youtube: string;
  records: Record[];
}

export interface Record {
  id: string;
  filename: string;
}
