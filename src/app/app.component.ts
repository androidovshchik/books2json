import { Component } from '@angular/core';
import {Database} from './app.models';
import * as clipboard from "clipboard-polyfill"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  db: Database = {
    config: {
      token: null,
      datetime: null,
      version: "0"
    },
    categories: []
  };

  constructor() {
    this.addCategory();
  }

  category: number = 0;

  book: number = 0;

  record: number = 0;

  onToken() {
    const date = new Date();
    this.db.config.datetime = ('0' + date.getDate()).slice(-2) + "." + ('0' + (date.getMonth() + 1)).slice(-2) + "." + date.getFullYear()
  }

  onUpCategory(i) {
    let newIndex;
    if (i - 1 < 0) {
      newIndex = AppComponent.move(this.db.categories, i, this.db.categories.length - 1);
    } else {
      newIndex = AppComponent.move(this.db.categories, i, i - 1);
    }
    this.category = newIndex;
    AppComponent.requestFocus("category", newIndex);
  }

  onDownCategory(i) {
    let newIndex;
    if (i + 1 > this.db.categories.length - 1) {
      newIndex = AppComponent.move(this.db.categories, i, 0);
    } else {
      newIndex = AppComponent.move(this.db.categories, i, i + 1);
    }
    this.category = newIndex;
    AppComponent.requestFocus("category", newIndex);
  }

  onUpBook(i) {
    let newIndex;
    if (i - 1 < 0) {
      newIndex = AppComponent.move(this.db.categories[this.category].books, i,
        this.db.categories[this.category].books.length - 1);
    } else {
      newIndex = AppComponent.move(this.db.categories[this.category].books, i, i - 1);
    }
    this.book = newIndex;
    AppComponent.requestFocus("book", newIndex);
  }

  onDownBook(i) {
    let newIndex;
    if (i + 1 > this.db.categories[this.category].books.length - 1) {
      newIndex = AppComponent.move(this.db.categories[this.category].books, i, 0);
    } else {
      newIndex = AppComponent.move(this.db.categories[this.category].books, i, i + 1);
    }
    this.book = newIndex;
    AppComponent.requestFocus("book", newIndex);
  }

  addCategory() {
    this.db.categories.unshift({
      id: AppComponent.generateHash(),
      name: null,
      books: [{
        id: AppComponent.generateHash(),
        name: null,
        author: null,
        reader: null,
        description: null,
        image: null,
        folder: null,
        duration: null,
        video: false,
        youtube: null,
        records: [{
          id: AppComponent.generateHash(),
          filename: null
        }]
      }]
    });
    this.category = 0;
    this.book = 0;
    this.record = 0;
    AppComponent.requestFocus("category", 0);
  }

  removeCategory() {
    this.db.categories.splice(this.category, 1);
    this.category -= this.category > 0 ? 1 : 0;
    this.book = 0;
    this.record = 0;
    AppComponent.requestFocus("category", this.category);
  }

  onCategoryChange(i) {
    this.category = i;
    this.book = 0;
    this.record = 0;
    AppComponent.requestFocus("category", this.category);
  }

  addBook() {
    this.db.categories[this.category].books.unshift({
      id: AppComponent.generateHash(),
      name: null,
      author: null,
      reader: null,
      description: null,
      image: null,
      folder: null,
      duration: null,
      video: false,
      youtube: null,
      records: [{
        id: AppComponent.generateHash(),
        filename: null
      }]
    });
    this.book = 0;
    this.record = 0;
    AppComponent.requestFocus("book", 0);
  }

  removeBook() {
    this.db.categories[this.category].books.splice(this.book, 1);
    this.book -= this.book > 0 ? 1 : 0;
    this.record = 0;
    AppComponent.requestFocus("book", this.book);
  }

  onBookChange(i) {
    this.book = i;
    this.record = 0;
    AppComponent.requestFocus("book", this.book);
  }

  addRecord() {
    this.db.categories[this.category].books[this.book].records.unshift({
      id: AppComponent.generateHash(),
      filename: null
    });
    this.record = 0;
    AppComponent.requestFocus("record", 0);
  }

  removeRecord() {
    this.db.categories[this.category].books[this.book].records.splice(this.record, 1);
    this.record -= this.record > 0 ? 1 : 0;
    AppComponent.requestFocus("record", this.record);
  }

  onRecordChange(i) {
    this.record = i;
    AppComponent.requestFocus("record", this.record);
  }

  onImport(event) {
    const file = event.target.files[0];
    if (file) {
      const reader = new FileReader();
      reader.readAsText(file, "UTF-8");
      reader.onload = (data: any) => {
        try {
          this.db = JSON.parse(data.target.result);
          this.category = 0;
          this.book = 0;
          this.record = 0;
        } catch (error) {
          console.log(error);
          alert("Ошибка при импорте файла");
        }
      };
      reader.onerror = error => {
        console.log(error);
      };
    }
  }

  onCopy() {
    const json = JSON.stringify(this.db, null, 2);
    try {
      clipboard.writeText(json)
        .catch(error => {
          console.error(error);
        });
    } catch (e) {
      console.error(e);
    }
  }

  onExport() {
    const json = JSON.stringify(this.db, null, 2);
    try {
      const element = document.createElement('a');
      element.setAttribute('href', "data:text/json;charset=UTF-8," + encodeURIComponent(json));
      element.setAttribute('download', "database.json");
      element.style.display = 'none';
      document.body.appendChild(element);
      element.click();
      document.body.removeChild(element);
    } catch (e) {
      console.error(e);
    }
  }

  static generateHash(length: number = 12): string {
    // ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789
    let chars = '2OJdYITUjgDXMoGBkl6r7K9HAzs5nCiqWNtuFScbfxmwh31pV48PLeQEyZ0vaR', hash = '';
    for (let c = 0; c < length; c++) {
      hash += chars.charAt(Math.floor(Math.random() * chars.length));
    }
    return hash;
  }

  static move(array, from, to): number {
    if (to === from) {
      return to;
    }
    const target = array[from];
    const increment = to < from ? -1 : 1;
    for (let k = from; k != to; k += increment){
      array[k] = array[k + increment];
    }
    array[to] = target;
    return to;
  }

  static requestFocus(name: string, i) {
    setTimeout(() => {
      try {
        document.getElementById(name + i).focus();
      } catch (error) {}
    }, 0);
  }
}
